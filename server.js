const express = require('express');

const app = express();
// Cors
const cors = require('cors');
// database
const { connection, close } = require('./lib/rethinkdb.js');
// books
const books = require('./routes/books');
// Middleware
app.use(cors());
// Body parser
app.use(express.json());

// Open connection
app.use(connection);
// Routes
app.use('/api/books', books);
// Close connection
app.use(close);

app.listen(3001, () => {
  console.log('Server is running on port 3001');
});
